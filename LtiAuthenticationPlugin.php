<?php

function exception_error_handler($severity, $message, $file, $line) {
    if (!(error_reporting() & $severity)) {
        // This error code is not included in error_reporting
        return;
    }
    throw new ErrorException($message, 0, $severity, $file, $line);
}
set_error_handler("exception_error_handler");


class LtiController extends Omeka_Controller_AbstractActionController {

    protected $_publicActions = ['launch'];
    
    protected $_auth;
    
    public function init() {

        $this->_helper->db->setDefaultModelName('LTIUser');
        $this->_auth = $this->getInvokeArg('bootstrap')->getResource('Auth');
            
        $this->_handlePublicActions();
    }
    
    protected function _handlePublicActions() {
        $action = $this->_request->getActionName();
        
        if (!in_array($action, $this->_publicActions)) {
            return;
        }
        
        if (is_admin_theme()) {
            $header = 'login-header';
            $footer = 'login-footer';
        } else {
            $heaeder = 'header';
            $footer = 'footer';
        }
        
        $this->view->header = $header;
        $this->view->footer = $footer;
        
        $actionFunction = $action.'Action';
        $this->$actionFunction();
    }

    private $ltiRolesToOmekaRoles = [
        'learner' => 'Contributor',
        'instructor' => 'Admin',
    ];
    
    
    /**
     * Generates a user name for Omeka
     * 
     * User names in Omeka cannot have certain characters (apostrophes, asterisks,
     * spaces, etc.). This function removes anynthing that is not a letter from
     * the user's name. It also appends a dash ('-') and the remote ID to the 
     * username just in case there are multiple people in the course with the
     * same first and last name.
     * 
     * 
     * @param string $fullName
     * @param string $userId
     * @return string
     */
    private function generateUsername($fullName, $userId) {
        $pattern = "/[^\\w]+/";
        return preg_replace($pattern, '', $fullName) . '-' . $userId;
    }

    /**
     * Handle the LTI Launch request.
     * 
     * If the user is not in the database, it creates a new record. If the user is in
     * the database, then it simply logs in the user. If all goes well, the user
     * is redirected to the admin page.
     * 
     * If not, the user sees a "Login failed" message
     * 
     * @throws Exception In case the URL is accessed some other way than through an LMS
     */
    public function launchAction() {
        
        if (!$this->getRequest()->isPost()) {
            throw new Exception("You must launch Omeka through an LTI-compatible LMS to use this feature.");
        }
        

        $postRequest = $_POST;

        $request = LTILaunchRequestFactory::fromArray($postRequest);
        
        
        // Look for user in database and start swapping out user
        $ltiRemoteInfo = $this->_helper->db->getTable('LTIAuthenticationUserRemoteInfo')->findByRemoteId(
            $request->toolConsumer->id, $request->user->remoteId
        );
        
        $user = null;
        
        
        if (empty($ltiRemoteInfo)) {
            $possibleUser = $this->_helper->db->getTable('User')->findByEmail($request->user->email);
            
            if ($possibleUser === null) {
                // The user is not in the database
	            
                $user = $this->createUser($request);
                $this->createRemoteInfo($user, $request);

            } else {
	    	    // The user was created through the Omeka interface, not an LTI request
	    	    // We must add remote info for that user
                $user = $possibleUser;
                $user->active = 1; // Necessary to activate user here, as it is possible a user may not have been activated yet. 
                                   // Using 1 instead of true as per Omeka's User.php class
                $user->save();
                $this->createRemoteInfo($user, $request);
            }
        } else {
        
            $user = $this->_helper->db->getTable('User')->find($ltiRemoteInfo->user_id);
            if (empty($user)) {
            	$ltiRemoteInfo->delete();
                $user = $this->_helper->db->getTable('User')->findByEmail($request->user->email);
                if (empty($user)) {
                    $this->createUser($request);
                }
                $this->createRemoteInfo($user, $request);
            }
        }
        
        $authAdapter = new LtiAuthentication_LtiAuthAdapter($user->id);

        $result = $this->_auth->authenticate($authAdapter);
                
        if (!$result->isValid()) {
            // If the login is invalid, something unforseeable has probably happened.
            throw new Exception("Login failure. Please contact your administrator.");
        } else {
            $this->_helper->redirector->gotoUrl('/admin/');
        }
    }

    private function createRemoteInfo($user, $request) {
        $userRemoteInfoRecord = new LtiAuthenticationUserRemoteInfo(null, $user->id, $request->user->remoteId, $request->toolConsumer->id);
        $userRemoteInfoRecord->save(true);
        return $userRemoteInfoRecord;
    }

    private function createUser($request) {
        $userRecord = new User();
        $role = '';
        if (array_search('instructor', $request->user->roles) > -1) {
            $role = 'super';
        } elseif (array_search('learner', $request->user->roles) > -1) {
            $role = 'contributor'; 
        } else {
            throw new Exception('LTI Role ' . implode(', ', $request->user->roles) . ' not defined for an Omeka role.');
        }
        
        $userRecord->email = $request->user->email;
        $userRecord->name = $request->user->fullName;
        $userRecord->active = 1; // Using 1 instead of true as per Omeka's User.php class
        $userRecord->username = $this->generateUsername($request->user->givenName.$request->user->familyName, $request->user->remoteId);
        $password = $request->user->givenName.$request->user->familyName. rand(0, 10000);
        $userRecord->setPassword($password);
        $userRecord->role = $role;
        
        $userRecord->save();
        return $userRecord;
    }
    
}



/**
 * Plugin Default class
 *
 * @author Dave Shepard, UCLA Center for Digital Humanitiest t
 */
class LtiAuthenticationPlugin extends Omeka_Plugin_AbstractPlugin {

    protected $_hooks = ['install', 'uninstall', 'define_routes'];

    
    public function hookInstall() {
        $db = $this->_db;

        $sql = "CREATE TABLE IF NOT EXISTS `$db->LtiAuthenticationUserRemoteInfo` ("
                . "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,"
                . "`user_id` INT UNSIGNED NOT NULL,"
                . "`tool_consumer_guid` VARCHAR(45) NOT NULL,"
                . "`tool_consumer_user_id` VARCHAR(45) NOT NULL,"
                . "PRIMARY KEY (`id`)"
                . ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
        ;

        $db->query($sql);
    }

    public function hookUninstall() {
        $db = $this->db;

        $sql = "DROP TABLE IF EXISTS `$db->LtiAuthenticationUserRemoteInfo`";
        $db->query($sql);
    }

    public function hookDefineRoutes($args) {
        $router = $args['router'];

        $router->addRoute(
            'lti', new Zend_Controller_Router_Route(
                'lti', 
                [
                    'controller' => 'lti',
                    'action' => 'launch',
                ]
            )
        );

    } 

}


class LtiAuthentication_LtiAuthAdapter implements Zend_Auth_Adapter_Interface {
    
    public function __construct($identity) {
        $this->_identity = $identity;
    }
    
    public function authenticate() {
        return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $this->_identity, ['it worked']);
    }
    
}

class LtiAuthentication_AuthResult extends Zend_Auth_Result {
    
    public function getIdentity() {
        return $this->_identity;
    }
}


class LTILaunchRequestFactory {
    
    public static function fromArray(array $postRequest) {
        $request = null;

        switch ($postRequest['lti_version']) {
            case 'LTI-1p0':
                $request = new LTIv1LaunchRequest($postRequest);
                break;
            default:
                throw new Exception("Unrecognized LTI version: $postRequest[lti_version]");
        }
        
        return $request;
    }
}

class LTIv1LaunchRequest {

    public $messageType, 
           $version
    ;

    public function __construct(array $request) {
        $this->messageType = $request['lti_message_type'];
        $this->version = $request['lti_version'];
        $this->resouceLink = new ResourceLink($request);
        $this->user = new UnvalidatedUser($request);
        $this->context = new UnvalidatedContext($request);
        $this->launchPresentation = new LaunchPresentation($request);
        $this->toolConsumer = new UnvalidatedToolConsumer($request);
    }
}

class ResourceLink {

    public $id, $title, $description;

    public function __construct(array $request) {
        $this->id = $request['resource_link_id'];
        $this->title = $request['resource_link_title'];
        $this->title = $request['resource_link_description'];
    }

}

/**
 * This is a stub user class, which represents a user before it had been located
 * into the database. Don't actually construct it outside of this package.
 */
class UnvalidatedUser {
    
    public $remoteId, $image, $roles, 
           $givenName, $familyName, $fullName, $email;

    public function __construct(array $request) {
        $this->remoteId = $request['user_id'];
        $this->image = $request['user_image'];
        $this->roles = array_map('strtolower', explode(',', $request['roles']));

        $this->givenName = $request['lis_person_name_given'];
        $this->familyName = $request['lis_person_name_family'];
        $this->fullName = $request['lis_person_name_full'];
        $this->email = $request['lis_person_contact_email_primary'];
    }
}

class UnvalidatedContext {

    public $remoteId, $type, $title, $label;
            
    public function __construct() {
        $this->remoteId = $request['context_id'];
        $this->type = $request['context_type'];
        $this->title = $request['context_title'];
        $this->label = $request['context_label'];
    }

}

class LaunchPresentation {

    public $locale, $target, $width, $height, $returnUrl;

    public function __construct(array $request) {
        $this->locale = $request['launch_presentation_url'];
        $this->target = $request['launch_presentation_documnet_target'];
        $this->width = $request['launch_presentation_width'];
        $this->height = $request['launch_presentation_height'];
        $this->returnUrl = $request['launch_presentation_return_url'];
    }

}

class UnvalidatedToolConsumer {
    
    public $id, $name, $description, $url, $contactEmail;

    public function __construct(array $request) {
        $this->id = $request['tool_consumer_instance_guid'];
        $this->name = $request['tool_consumer_instance_name'];
        $this->description = $request['tool_consumer_instance_description'];
        $this->url = $request['tool_consumer_instance_url'];
        $this->contactEmail = $request['tool_consumer_instance_contact_email'];
    }
}


class LtiAuthenticationUserRemoteInfo extends Omeka_Record_AbstractRecord implements Zend_Acl_Resource_Interface {
    public $user_id;
    public $tool_consumer_guid;
    public $tool_consumer_user_id;

    public function __construct($id, $userId, $remoteId, $toolConsumerURL) {
        parent::__construct();
        $this->id = $id;
        $this->user_id = $userId;
        $this->tool_consumer_guid = $toolConsumerURL;
        $this->tool_consumer_user_id = $remoteId;
    }

    public function getResourceId() {
        return 'LtiAuthenticationUserRemoteInfo';
    }
    
    public static function fromRecord(array $record) {
    	return new LtiAuthenticationUserRemoteInfo(
    	    $record['id'],
    	    $record['user_id'],
    	    $record['tool_consumer_guid'],
    	    $record['tool_consumer_user_id']
    	);
    }

}

class Table_LtiAuthenticationUserRemoteInfo extends Omeka_Db_Table {

    public function findUserByLocalId($id) {
    	$select = $this->getSelect();
        $users = $select->where('user_id = ?', $id)->query()->fetchAll();
        
        if (empty($users)) {
            return [];
        } elseif (count($users) === 1) {
            return $users[0];
        } else {
            // TODO: Log the existence of multiple remote IDs for one user
            return $users[0];
        }
    }

    public function findByRemoteId($toolConsumerGuid, $userRemoteId) {
        $select = $this->getSelect();
        
        $users = $select
                ->where('tool_consumer_guid = ?', $toolConsumerGuid)
                ->where('tool_consumer_user_id = ?', $userRemoteId)
                ->query()
                ->fetchAll();
        
        if (empty($users)) {
            return [];
        } else{
            if (count($users) !== 1) {
	        // TODO: Log existence of multiple users
            }
            $foundUser = LtiAuthenticationUserRemoteInfo::fromRecord($users[0]);            
            return $foundUser;
        }
    }

}

class ToolConsumer {

    private $instanceId;
    private $instanceName;
    private $instanceDescription;
    private $instanceUrl;
    private $toolConsumerInstanceContactEmail;

    private $_accessibleFields = [
        'instanceId', 'instanceName', 'instanceDescription', 'instanceUrl', 'toolConsumerInstanceContactEmail'
    ];

    public function __construct($instanceId, $instanceName, $instanceDescription, $instanceUrl, $toolConsumerInstanceContactEmail) {
        $this->instanceId = $instanceId;
        $this->instanceName = $instanceName;
        $this->instanceDescription = $instanceDescription;
        $this->instanceUrl = $instanceUrl;
        $this->toolConsumerInstanceContactEmail;
    }

    public static function fromRequest(LTIv1LaunchRequest $request) {
        $unvalidatedToolConsumer = $request->toolConsumer;
        return new ToolConsumer(
            $unvalidatedToolConsumer->id, 
            $unvalidatedToolConsumer->name, 
            $unvalidatedToolConsumer->description, 
            $unvalidatedToolConsumer->url, 
            $unvalidatedToolConsumer->contactEmail
        );
    }

    public function __get($name) {
        if (array_search($name, $this->_accessibleFields) > -1) {
            return $this->$name;
        } else {
            return null;
        }
    }

}


