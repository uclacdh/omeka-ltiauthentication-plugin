# Omeka LTI Authentication Plugin #

Enables [Omeka](https://omeka.org/) to recognize users from an [LTI](http://www.imsglobal.org/activity/learning-tools-interoperability)-compatible Learning Management System.

### How do I get set up? ###

Aside from Omeka and an LMS, this plugin doesn't have any external dependencies for development. Simply clone the repository:

`git clone git@bitbucket.org:uclacdh/omeka-ltiauthentication-plugin.git`

Please note the following:

  * If a user with the same email address already exists in Omeka, Omeka, this plugin will take ownership of that user's account. That user's password should still work correctly. It may be complicated to remove a user who has logged in through the LMS.
  * If a user's account is created, but not activated, and the user then logs in through the LMS, their account will be automatically activated. This is to avoid inconveniences.
  * A new user's role is set the first time they log in in through the LMS. After that, roles must be updated through the Omeka interface. This is by design.
  * One implication of all of the above is that if a user's account is valid in the LMS, their account can never be really deleted from Omeka. Instead, he or she must have their account removed from the LMS first, and then from Omeka.

### Deployment instructions ###
See [Omeka's documentation on managing plugins](http://omeka.org/codex/Managing_Plugins_2.0)


### Configuring your LMS ###

The LTI launch URL (which you'll use in your LMS) is {http or https}://{root of your Omeka installation}/lti/launch
For example, if your Omeka site is at https://example.com/omeka/, then the URL
would be https://example.com/omeka/lti/launch.

If the LMS you are using uses HTTPS, the Omeka site must also use HTTPS. This is a restriction that most, if not all, browsers enforce.

Please note that this plugin currently does not support OAuth features.


### Credits ###

This plugin was developed by the [UCLA Center for Digital Humanities](https://cdh.ucla.edu/). 
Copyright 2016 The Regents of the University of California.